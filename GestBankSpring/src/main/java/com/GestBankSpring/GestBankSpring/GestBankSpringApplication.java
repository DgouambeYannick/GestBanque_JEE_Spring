package com.GestBankSpring.GestBankSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestBankSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestBankSpringApplication.class, args);
	}
}
