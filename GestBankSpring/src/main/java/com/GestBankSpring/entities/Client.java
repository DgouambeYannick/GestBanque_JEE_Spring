/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestBankSpring.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Table(name="CLIENT")
public class Client implements Serializable{ 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeclient;

    private String nomclient;
    private String adresseclient;
    private String photoclient;

    @OneToMany(mappedBy = "client", fetch=FetchType.LAZY)  //Lazy interdit la recuperation de la collection a lappel de la classe sauf en cas de get
    private Collection<Compte> Compte;

    public Client(String nomclient, String adresseclient) {
        this.nomclient = nomclient;
        this.adresseclient = adresseclient;
    }

    public Client() {
    }

    
    public Long getCodeclient() {
        return codeclient;
    } 

    public void setCodeclient(Long codeclient) {
        this.codeclient = codeclient;
    }

    public String getNomclient() {
        return nomclient;
    }

    public void setNomclient(String nomclient) {
        this.nomclient = nomclient;
    }

    public String getAdresseclient() {
        return adresseclient;
    }

    public void setAdresseclient(String adresseclient) {
        this.adresseclient = adresseclient;
    }
    @XmlTransient
    public Collection<Compte> getCompte() {
        return Compte;
    }

    public void setCompte(Collection<Compte> Compte) {
        this.Compte = Compte;
    }
    
     public String getPhotoclient() {
        return photoclient;
    }

    public void setPhotoclient(String photoclient) {
        this.photoclient = photoclient;
    }
}
