/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestBankSpring.entities;

import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author HP
 */
@Entity
@DiscriminatorValue("CompteCourant")
public class CompteCourant extends Compte{
    private double decouvert;

    public CompteCourant() {
        super();
    }

    public CompteCourant(double decouvert, String codeCompte, Date Datecreate, double solde) {
        super(codeCompte, Datecreate, solde);
        this.decouvert = decouvert;
    }

    public double getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }
    
}
