/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestBankSpring.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
public class User implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeEmploye;
    
    private String nomEmploye;
    private Integer enabled;
    private String name;
    private String password;
    private String photouser;
    
    @ManyToOne
    @JoinColumn(name="ID_EMP_SUP")
    private User employeSup;
    @ManyToMany
    @JoinTable(name="user_role",joinColumns = @JoinColumn(name="users_id"), inverseJoinColumns = @JoinColumn(name="roles_id"))
    private Collection<Role> roles;

    public User() {
    }

    public User(String nomEmploye, String name, String password) {
        this.nomEmploye = nomEmploye;
        this.name = name;
        this.password = password;
    }

    public Long getCodeEmploye() {
        return codeEmploye;
    }

    public void setCodeEmploye(Long codeEmploye) {
        this.codeEmploye = codeEmploye;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public User getEmployeSup() {
        return employeSup;
    }

    public void setEmployeSup(User employeSup) {
        this.employeSup = employeSup;
    }
    @XmlTransient
    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getenabled() {
        return enabled;
    }

    public void setenabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getPhotouser() {
        return photouser;
    }

    public void setPhotouser(String photouser) {
        this.photouser = photouser;
    }
    
    
}
 