/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.GestBankSpring.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) //signifit que tous les types de comptes seront saved dans la meme table
@DiscriminatorColumn(name="TYPE_CPTE", discriminatorType = DiscriminatorType.STRING,length = 20)  // permet de creer le champ prendra la valeur des classes filles
public abstract class Compte implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idompte;
    
    private String codeCompte;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date Datecreate;
    private double solde;
    @ManyToOne
    @JoinColumn(name="ID_CLI")
    private Client client;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;
    @OneToMany(mappedBy = "compte",fetch = FetchType.LAZY)
    private Collection<Operation> operations;

    public Compte() {
    }

    public Compte(String codeCompte, Date Datecreate, double solde) {
        this.codeCompte = codeCompte;
        this.Datecreate = Datecreate;
        this.solde = solde;
    }

    public Long getIdompte() {
        return idompte;
    }

    public void setIdompte(Long idompte) {
        this.idompte = idompte;
    }

    public String getCodeCompte() {
        return codeCompte;
    }

    public void setCodeCompte(String codeCompte) {
        this.codeCompte = codeCompte;
    }

    public Date getDatecreate() {
        return Datecreate;
    }

    public void setDatecreate(Date Datecreate) {
        this.Datecreate = Datecreate;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    @XmlTransient
    public Collection<Operation> getOperations() {
        return operations;
    }

    public void setOperations(Collection<Operation> operations) {
        this.operations = operations;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
    
    
}
